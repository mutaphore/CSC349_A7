import java.util.*;

public class CounterfeitDetect {
   
   private ArrayList<Integer> trans;
   private int index;
   private int count;
   
   public CounterfeitDetect(ArrayList<Integer> arr) {
      trans = new ArrayList<Integer>(arr);
      count = 0;
   }
   
   public int scan(ArrayList<Integer> trans) {
      int num1, num2, rtn, n = trans.size();
      ArrayList<Integer> sub1, sub2;
      
      if (n > 1) {
         sub1 = new ArrayList<Integer>(trans.subList(0, n / 2));
         sub2 = new ArrayList<Integer>(trans.subList(n / 2, n)); 
         num1 = scan(sub1);
         num2 = scan(sub2);
         
         if (num1 == -1 && num2 == -1)
            rtn = -1;  //there is no more frequent element
         else
            rtn = checkMoreThanHalf(num1, num2, trans);
      }
      else
         rtn = trans.get(0);
      
      return rtn;
   }
   
   private int checkMoreThanHalf(int num1, int num2, ArrayList<Integer> trans) {
      int count1 = 0, count2 = 0, ndx1 = 0, ndx2 = 0, n = trans.size();

      for (int i = 0; i < n; i++) {
         if (trans.get(i) == num1) {
            count1++;
            ndx1 = i;
         }
         if (trans.get(i) == num2) {
            count2++;
            ndx2 = i;
         }
      }
      if (count1 > n / 2) {
         count = count1;
         index = ndx1;
         return num1;
      }
      else if (count2 > n / 2) {
         count = count2;
         index = ndx2;
         return num2;
      }
      else
         return -1;  //there is no more frequent element
   }
   
   public void printResult() {
      
      System.out.println("Threshold for fraudulent transactions is " + trans.size() / 2);
      System.out.print("The number of identical transactions was ");
      if (count > trans.size() / 2) {
         System.out.println(count);
         System.out.println("The index of one of the identical transaction credentials is: " + index);
      }
      else
         System.out.println("insufficient");
   }
}