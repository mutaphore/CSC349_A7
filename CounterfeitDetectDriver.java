import java.util.*;
import java.io.*;

public class CounterfeitDetectDriver {
   
   public static void main(String args[]) {
      Scanner sc = new Scanner(System.in);
      String fileName;
      FileInputStream fileIn;
      ArrayList<Integer> trans = new ArrayList<Integer>();
      CounterfeitDetect cfd;
      int n;

      try {
         System.out.print("Enter input file: ");
         fileName = sc.next();
         fileIn = new FileInputStream(new File(fileName));
      }
      catch (FileNotFoundException e) {
         System.out.println("File not found!");
         return;
      }
      
      sc = new Scanner(fileIn);
      n = sc.nextInt();
      sc.nextLine();
      while (sc.hasNextInt())
         trans.add(sc.nextInt());      
      
      cfd = new CounterfeitDetect(trans);
      cfd.scan(trans);
      Iterator itr = trans.listIterator();
      while (itr.hasNext())
         System.out.print(itr.next() + " ");
      System.out.println();
      cfd.printResult();
  }
  
}